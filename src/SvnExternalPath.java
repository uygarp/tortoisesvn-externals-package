
public class SvnExternalPath
{
	String Link;
	String Folder;
	
	public SvnExternalPath(String link, String folder)
	{
		Link = link;
		Folder = folder;
	}
	
	public void setLink(String link)
	{
		Link = link;
	}	
	
	public String getLink()
	{
		return Link;
	}
	
	public String getFolder()
	{
		return Folder;
	}
	
	public void setFolder(String folder)
	{
		Folder = folder;
	}
}
