import java.util.ArrayList;
import java.util.List;


public class TortoiseProcArgParser
{
	private static TortoiseProcArgParser instance = null;
	
	public static TortoiseProcArgParser getInstance()
	{
	   if(instance == null)
	   {
	      instance = new TortoiseProcArgParser();
	   }
	   return instance;
	}

    public SvnCommandPath getPath(List<String> Args)
    {
    	SvnCommandPath svnPath = null;

    	for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).substring(0, 6).equalsIgnoreCase("/path:"))
			{
				svnPath = new SvnCommandPath();
				svnPath.setPath(Args.get(i).substring(6).replace(" ", ""));
				break;
			}
		}    	
		
		
    	return svnPath;
    }
    
    private boolean isURL(String path)
    {
    	for(int i = 0; i < GLOBAL.UrlHeaders.length; i++)
    	{
    		if(path.startsWith(GLOBAL.UrlHeaders[i]))
    		{
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public boolean hasVersionNoQuiet(List<String> Args)
    {
		if(Args == null || Args.size() != 1)
		{
			return false;
		}
		
		if(!Args.get(0).equals("--version"))
		{
			return false;
		}
		
		return true;
    }
    
    public boolean hasLogArg(List<String> Args)
    {
		if(Args == null || Args.size() < 1)
		{
			return false;
		}
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).equalsIgnoreCase("/command:log"))
			{
				return true;
			}
		} 
		
		return false;
    }
	
	public List<String> getArgsWithoutPath(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		
		for(int i = 0; i < Args.size(); i++)
		{
			boolean argIsPath = false;
			if(!Args.get(i).startsWith("-"))
			{
				if(i == 2)
				{
					argIsPath = true;
				}
				else if(i > 2)
				{
					if(Args.get(i - 1).equals("-r") ||
						Args.get(i - 1).equals("--revision") ||
						Args.get(i - 1).equals("-c") ||
						Args.get(i - 1).equals("--change") ||
						Args.get(i - 1).equals("--targets") ||
						Args.get(i - 1).equals("-l") ||
						Args.get(i - 1).equals("--limit") ||
						Args.get(i - 1).equals("--with-revprop") ||
						Args.get(i - 1).equals("--depth") ||
						Args.get(i - 1).equals("--diff-cmd") ||
						Args.get(i - 1).equals("-x") ||
						Args.get(i - 1).equals("--extentions") ||
						Args.get(i - 1).equals("--username") ||
						Args.get(i - 1).equals("--password") ||
						Args.get(i - 1).equals("--config-dir") ||
						Args.get(i - 1).equals("--config-option"))
					{
						argIsPath = false;
					}
					else
					{
						argIsPath = true;
					}
				}
			}
			
			if(!argIsPath)
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		return tempArgs;
	}
	
	
	public String setCommand(List<String> Args)
	{
		String tempCommand = "";
		
		for(int i = 0; i < Args.size(); i++)
		{
			tempCommand += " " + Args.get(i);
		}
		
		return tempCommand;
	}
}

