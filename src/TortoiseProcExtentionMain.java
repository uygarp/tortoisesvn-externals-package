import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class TortoiseProcExtentionMain
{
	private static SvnCmd svn = SvnCmd.getInstance();
	private static TortoiseProcCmd tortoise = TortoiseProcCmd.getInstance();
	private static SvnProcessor svnProc = SvnProcessor.getInstance();
	private static SvnXMLParser svnXMLParser = SvnXMLParser.getInstance();
	private static TortoiseProcArgParser tortoiseArgParser = TortoiseProcArgParser.getInstance();
	private static SvnCommandPath userPath = null;
		
	protected static final long invalid = -2;
	
	public static void main(String[] args)
			throws IOException, InterruptedException, ParserConfigurationException, SAXException, ParseException 
	{
		if(GLOBAL.DEBUG)
		{
			//String path = "http://svn/repos/SingleChipTV/branches/projects/Genki";
			//String path = "http://svn/repos/SingleChipTV/branches/projects/Artwin";
			//String path = "http://svn/repos/SingleChipTV/branches/projects/Toros_prod_phase1";
			//String path = "http://svn/repos/SingleChipTV/branches/Phoenix";
			//String path = "D:\\Workspace\\Phoenix_OBS";
			String path = "D:\\Workspace\\Genki\\app_dtv";
	
			//String[] tempArgs = {"svnpure", "--version"};
			//String[] tempArgs = {"svnpure", "--version", "--quiet"};
			//String[] tempArgs = {"svnpure", "info", "D:\\Workspace\\Phoenix_OBS", "--xml"};
			//String[] tempArgs = {"svnpure", "--version"};
			//String [] tempArgs = {"C:\\Program Files\\TortoiseSVN\\bin\\svnold"};
			//String[] tempArgs = {"svnpure", "log", "-l3", path};
			String[] tempArgs = {"TortoiseProc", "svnpure.exe",
								"/command:log",
								"/path:http://svn/repos/SingleChipTV/branches/projects/Genki",
								"/startrev:HEAD",
								"/propspath:D:\\Workspace\\Phoenix_OBS"};
			
			args = tempArgs;
		}
		
		int limit = 10;
		
		if(args == null || args.length < 1)
		{
			println("Error in Command Args... [args is null or empty]");
			System.exit(GLOBAL.EXIT_FAILURE);
		}
		
		tortoise.setCmd(args[0]);
		svn.setCmd(args[1]);
		
		List<String> Args = getTortoiseProcArgs(args);
		
		if(!tortoiseArgParser.hasLogArg(Args))
		{
			println("Error in Command Args... [tortoiseArgParser.isLogCommandArgs(Args)]");
			RunTortoiseProcAndExit(Args);
		}
		
		userPath = tortoiseArgParser.getPath(Args);		
		if(userPath == null || userPath.getPath().equals(""))
		{
			println("Error in User Path... [tortoiseArgParser.getPath(Args)]");
			RunTortoiseProcAndExit(Args);
		}
		
		SvnInfo info = getSvnInfo(userPath);
		if(info == null)
		{
			println("Error in Svn Info... [getSvnInfo(userPath)]");
			RunTortoiseProcAndExit(Args);
		}
		
		userPath.setSvnInfo(info);
		
		List<SvnExternalPath> externalPathList =  getExternalPaths(userPath.getSvnInfo().getURL());
		if(externalPathList.size() == 0)
		{
			println("No External paths found, run Pure SVN command... [getExternalPaths(targetPath.getPath())]");
			RunTortoiseProcAndExit(Args);
		}
		
		SvnCommandPath targetPath =  normalizeExternalPaths(userPath, externalPathList);//URL
		if(targetPath == null || targetPath.getPath().equals(""))
		{
			println("Error occurred while normalizing external paths... [normalizeExternalPaths(userPath, externalPathList);]");
			RunTortoiseProcAndExit(Args);
		}
			
		/// RunTortoiseProcAndExit(Args);
		
		String command = svn.getCmd() + " log --verbose --xml --limit " + limit + " " + targetPath.getPath();
		for(int i = 0; i < targetPath.getChildDirectories().size(); i++)
		{
			command += " " + targetPath.getChildDirectories().get(i);
		}
		
		List<SvnLog> list =  getLogList(command);
		
		//Finish
		
		try
		{
			TortoiseProcExtentionWindow newTortoiseWindow = new TortoiseProcExtentionWindow();
			newTortoiseWindow.setLogList(list);
			newTortoiseWindow.open();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		System.exit(GLOBAL.EXIT_SUCCESS);
	}
	
	private static List<SvnExternalPath> getExternalPaths(String path) throws IOException, ParserConfigurationException, SAXException
	{
		String rawXML = svnProc.runSvnCommand(svn.getCmd() + "propget svn:externals --xml " + path);	
		if(errorCheckOfCommandOutput(rawXML))
		{
			return null;
		}
		else
		{
			return svnXMLParser.getExternalPathsFromXML(rawXML);
		}
	}
	
	private static SvnInfo getSvnInfo(SvnCommandPath path) throws IOException, ParserConfigurationException, SAXException, ParseException
	{
		String rawXML = svnProc.runSvnCommand(svn.getCmd() + "info --xml " + path.getPath());		
		if(errorCheckOfCommandOutput(rawXML))
		{
			return null;
		}
		else
		{
			return svnXMLParser.getInfoFromXML(rawXML);
		}
	}
	
	private static List<SvnLog> getLogList(String command) throws IOException, ParserConfigurationException, SAXException, ParseException
	{
		String rawXML = svnProc.runSvnCommand(command);
		if(errorCheckOfCommandOutput(rawXML))
		{
			return null;
		}
		else
		{
			return svnXMLParser.getLogListFromXML(rawXML);
		}		
	}

	private static boolean errorCheckOfCommandOutput(String output)
	{
		BufferedReader outputReader = new BufferedReader(new StringReader(output));
		String outputLine = "";
		try
        {
			outputLine = outputReader.readLine();
	        
	        while(outputLine != null)
	        {
	        	if(outputLine.startsWith("svn:"))
	        	{
	        		return true;
	        	}
	        	outputLine = outputReader.readLine();
	        }
        }
		catch (IOException e)
        {
	        return true;
        }		
		
		return false;
	}
	
	private static String getTestCommand(List<String> Args, SvnCommandPath path)
	{
		String commandString = svn.getCmd();
		boolean pathAdded = false;
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(!pathAdded && Args.get(i).equals(path.getPath()))
			{
				commandString += path.getSvnInfo().getURL() + " ";
				pathAdded = true;
			}
			else
			{
				if(pathAdded)
				{
					int index = path.getChildDirectories().indexOf(Args.get(i));
					if(index == -1)
					{
						commandString += Args.get(i) + " ";
					}
					
				}
				else
				{
					commandString += Args.get(i) + " ";
				}
			}
		}		
		
		return commandString;		
	}
	
	static SvnCommandPath normalizeExternalPaths(SvnCommandPath commandPath, List<SvnExternalPath> externalPathList)
	{
		SvnCommandPath path = null;
		
		if(externalPathList != null || commandPath != null)
		{
			path =  new SvnCommandPath();
			List<SvnExternalPath> extPathListToGet = new ArrayList<SvnExternalPath>();
			
			if(commandPath.getChildDirectories().size() > 0)
			{
				for(int i = 0; i < commandPath.getChildDirectories().size(); i++)
				{
					boolean found = false;
					for(int j = 0; j < externalPathList.size(); j++)
					{
						String compare1 = commandPath.getChildDirectories().get(i);
						String compare2 = externalPathList.get(j).getFolder();
						if(compare1.equals(compare2))
						{
							extPathListToGet.add(externalPathList.get(j));
							found = true;
							break;
						}
					}
					
					if(!found)
					{
						extPathListToGet = null;
						break;
					}
				}
			}
			else
			{
				extPathListToGet = externalPathList;
			}
			
			if(extPathListToGet != null && extPathListToGet.size() > 0)
			{
				String correctCommonFolder = "";
				String checkCommonFolder = "";
				String[] folderPartArray = extPathListToGet.get(0).getLink().split("\\/");
	
				FOLDERPARTCHECK: for(int i = 0; i < folderPartArray.length; i++)
				{
					checkCommonFolder += folderPartArray[i] + "/";
					
					for(int j = 0; j < extPathListToGet.size(); j++)
					{
						if(!extPathListToGet.get(j).getLink().startsWith(checkCommonFolder))
						{
							break FOLDERPARTCHECK;
						}
					}
					
					correctCommonFolder = checkCommonFolder;
				}
				
				path.setPath(commandPath.getSvnInfo().getRepoRoot() + correctCommonFolder);
							
				for(int i = 0; i < extPathListToGet.size(); i++)
				{
					String newFolder = extPathListToGet.get(i).getLink().substring(correctCommonFolder.length());
					path.addChildDirectory(newFolder);
				}
			}
		}
		
		return path;
	}	
	
	static List<String> removeUserPathsFromArgs(List<String> args, SvnCommandPath path)
	{
		List<String> newArgs = new ArrayList<String>();
		
		if(!path.getPath().equals(""))
		{
			for(int i = 0; i < args.size(); i++)
			{
				boolean isPathMember = false;
				
				if(args.get(i).equals(path.getPath()))
				{
					isPathMember = true;
				}
				else
				{
					for(int j = 0; j < path.getChildDirectories().size(); j++)
					{
						if(args.get(i).equals(path.getChildDirectories().get(j)))
						{
							isPathMember = true;
							break;
						}
					}
				}
				
				
				if(!isPathMember)
				{
					newArgs.add(args.get(i));
				}
			}
		}		
		
		return newArgs;
	}
	
	static SvnCommandPath convertURLCommandPath(SvnCommandPath path) throws IOException, ParserConfigurationException, SAXException, ParseException
	{
		SvnCommandPath urlPath = null;
		if(path != null)
		{
			urlPath = new SvnCommandPath();
			String pathString = "";
			if(path.getPath().equals(""))
			{
				//TODO: Check for directory slashes
				//TODO: Check this also for Linux.
				//String _path = System.getProperty("user.dir").replace("\", "/");
				pathString = System.getProperty("user.dir");
				println("Target Directory = " + System.getProperty("user.dir"));
			}
			else if(path.isLocalDirectory())
			{
				pathString = path.getSvnInfo().getURL();
			}
			else
			{
				pathString = path.getPath();
			}
			
			urlPath.setPath(pathString);
			
			for(int i = 0; i < path.getChildDirectories().size(); i++)
			{
				urlPath.addChildDirectory(path.getChildDirectories().get(i));
			}
			
			urlPath.setSvnInfo(path.getSvnInfo());
		}
		
		return urlPath;
	}

	private static void RunTortoiseProcAndExit(List<String> Args) throws IOException
	{
		String command = tortoise.getCmd();
		
		if(Args != null)
		{
			for(int i = 0; i < Args.size(); i++)
			{
				command += " " + Args.get(i);
			}
		}
		
		System.exit(svnProc.runCommand(command));
	}
	
	private static List<String> getTortoiseProcArgs(String[] argList)
	{
		List<String> Args = null;
		
		if(argList != null)
		{
			Args = new ArrayList<String>();
			for(int i = 2; i < argList.length; i++)
			{
				println("argList[i]=" + argList[i]);
				Args.add(argList[i]);
			}
		}
		
		return Args;
	}
	
	private static void println(String str)
	{
		if(GLOBAL.DEBUG)
		{
			System.out.println(str);
		}
	}	
}


