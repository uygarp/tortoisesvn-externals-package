import java.util.Date;
import java.util.List;

public class SvnLog
{
	public long Revision;
	public String Author;
	public Date DateTime;
	public String Message;
	public List<SvnChangedPath> Changes;
	
	SvnLog(long revision, String author, Date dateTime, String message, List<SvnChangedPath> changes)
	{
		Revision = revision;
		Author = author;
		DateTime =  dateTime;
		Message =  message;
		Changes =  changes; 
	}
}