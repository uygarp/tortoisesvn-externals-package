public interface GLOBAL
{
	boolean DEBUG = true;
	
	public int EXIT_SUCCESS  =  0;
	public int EXIT_FAILURE  =  1;
	
	String[] UrlHeaders = {"file:///", "http://", "https://", "svn://", "svn+ssh://"};
}
