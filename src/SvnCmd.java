
public class SvnCmd
{
	String svnCmd = "svn ";
	
	private static SvnCmd instance = null;

	public static SvnCmd getInstance()
    {
       if(instance == null)
       {
          instance = new SvnCmd();
       }
       return instance;
    }

	public void setCmd(String value)
	{
		svnCmd = value + " ";
	}
	
	public String getCmd()
	{
		return svnCmd;
	}
}
