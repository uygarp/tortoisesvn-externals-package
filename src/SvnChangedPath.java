class SvnChangedPath
{
	String Action;
	String PropMods;
	String TextMods;
	String Kind;
	String Value;
	//ChangeType Type;
	
	SvnChangedPath(String action, String propMods, String textMods, String kind, String value)
	{
		Action = action;
		PropMods = propMods;
		TextMods = textMods;
		Kind = kind;
		Value = value;
	}
}

/*
enum ChangeType
{
	NoChange,
	Modified,
	Added,
	Deleted,
	Replaced
}
*/