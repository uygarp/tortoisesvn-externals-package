import java.util.Date;

public class SvnInfo
{
	private String entry_path = "";
	private String entry_kind = "";
	private Long entry_revision;
	
	private String url = "";
	private String repo_root = "";
	private String repo_uuid = "";
	
	private Long commit_revision;
	private String commit_author = "";
	private Date commit_date;
	
	public void setPath(String value)
	{
		entry_path = value;
	}
	
	public String getPath()
	{
		return entry_path;
	}
	
	public void setKind(String value)
	{
		entry_kind = value;
	}
	
	public String getKind()
	{
		return entry_kind;
	}
	
	public void setRevision(Long value)
	{
		entry_revision = value;
	}
	
	public Long getRevision()
	{
		return entry_revision;
	}
	
	public void setURL(String value)
	{
		url = value;
	}
	
	public String getURL()
	{
		return url;
	}
	
	public void setRepoRoot(String value)
	{
		repo_root = value;
	}
	
	public String getRepoRoot()
	{
		return repo_root;
	}
	
	public void setRepoUUID(String value)
	{
		repo_uuid = value;
	}
	
	public String getRepoUUID()
	{
		return repo_uuid;
	}
	
	public void setCommitRevision(Long value)
	{
		commit_revision = value;
	}
	
	public Long getCommitRevision()
	{
		return commit_revision;
	}
	
	public void setCommitAuthor(String value)
	{
		commit_author = value;
	}
	
	public String getCommitAuthor()
	{
		return commit_author;
	}
	
	public void setCommitDate(Date value)
	{
		commit_date = value;
	}
	
	public Date getCommitDate()
	{
		return commit_date;
	}	
}
