import java.util.ArrayList;
import java.util.List;


public class SvnCommandPath
{
	private String mainPath;
	private List<String> childDirs;
	private SvnInfo svnInfo = null;
	
	SvnCommandPath()
	{
		mainPath = "";
		childDirs = new ArrayList<String>();
	}
	
	public void setPath(String path)
	{
		mainPath = path;
	}
	
	public String getPath()
	{
		return mainPath;
	}
	
	public void addChildDirectory(String dirName)
	{
		if(childDirs == null)
		{
			childDirs = new ArrayList<String>();
		}
		
		childDirs.add(dirName);
	}
	
	public List<String> getChildDirectories()
	{
		return childDirs;
	}
	
	public boolean isLocalDirectory()
	{
    	for(int i = 0; i < GLOBAL.UrlHeaders.length; i++)
    	{
    		if(mainPath.startsWith(GLOBAL.UrlHeaders[i]))
    		{
    			return false;
    		}
    	}

    	return true;
	}
	
	public void setSvnInfo(SvnInfo info)
	{
		svnInfo = info;
	}
	
	public SvnInfo getSvnInfo()
	{
		return svnInfo;
	}
}
