import java.util.ArrayList;
import java.util.List;


public class SvnArgParser
{
	private static SvnArgParser instance = null;
	
	protected static final long invalid = -2;
	
	private static String path = "";
	
	protected SvnArgParser()
	{		
	}

	public static SvnArgParser getInstance()
	{
	   if(instance == null)
	   {
	      instance = new SvnArgParser();
	   }
	   return instance;
	}
	
    public String getPath()
    {
    	return path;
    }
    
    public void setPath(String value)
    {
    	path = value;
    }
	
    public SvnCommandPath getPath(List<String> Args)
    {
    	SvnCommandPath svnPath = new SvnCommandPath();

    	boolean pathFound = false;
    	
		for(int i = 0; i < Args.size(); i++)
		{
			boolean couldBePath = false;
			if(!Args.get(i).startsWith("-"))
			{
				if(i == 1)
				{
					couldBePath = true;
				}
				else if(i > 1)
				{
					if(Args.get(i - 1).equals("-r") ||
						Args.get(i - 1).equals("--revision") ||
						Args.get(i - 1).equals("-c") ||
						Args.get(i - 1).equals("--change") ||
						Args.get(i - 1).equals("--targets") ||
						Args.get(i - 1).equals("-l") ||
						Args.get(i - 1).equals("--limit") ||
						Args.get(i - 1).equals("--with-revprop") ||
						Args.get(i - 1).equals("--depth") ||
						Args.get(i - 1).equals("--diff-cmd") ||
						Args.get(i - 1).equals("-x") ||
						Args.get(i - 1).equals("--extentions") ||
						Args.get(i - 1).equals("--username") ||
						Args.get(i - 1).equals("--password") ||
						Args.get(i - 1).equals("--config-dir") ||
						Args.get(i - 1).equals("--config-option"))
					{
						couldBePath = false;
					}
					else
					{
						couldBePath = true;
					}
				}
			}
			
			if(couldBePath)
			{
				if(pathFound && (isURL(Args.get(i)) || !isURL(svnPath.getPath())))
				{
					svnPath = null;
					break;
				}
				
				if(pathFound)
				{
					svnPath.addChildDirectory(Args.get(i));
				}
				else
				{
					svnPath.setPath(Args.get(i));
					pathFound = true;					
				}
			}			
		}    	
    	
		
		
    	return svnPath;
    }
    
    private boolean isURL(String path)
    {
    	for(int i = 0; i < GLOBAL.UrlHeaders.length; i++)
    	{
    		if(path.startsWith(GLOBAL.UrlHeaders[i]))
    		{
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public boolean hasVersionNoQuiet(List<String> Args)
    {
		if(Args == null || Args.size() != 1)
		{
			return false;
		}
		
		if(!Args.get(0).equals("--version"))
		{
			return false;
		}
		
		return true;
    }
    
    public boolean hasLogArg(List<String> Args)
    {
		if(Args == null || Args.size() < 1)
		{
			return false;
		}
		
		if(!Args.get(0).equals("log"))
		{
			return false;
		}
		
		return true;
    }
    
	public static String getCommandWithoutLRC(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		String tempCommand = "";
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).substring(0, 2).equals("-l"))
			{
				if(Args.get(i).equals("-l"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else if(Args.get(i).startsWith("--limit"))
			{
				if(Args.get(i).equals("--limit"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else if(Args.get(i).substring(0, 2).equals("-r"))
			{
				if(Args.get(i).equals("-r"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else if(Args.get(i).startsWith("--revision"))
			{
				if(Args.get(i).equals("--revision"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else if(Args.get(i).substring(0, 2).equals("-c"))
			{
				if(Args.get(i).equals("-c"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else if(Args.get(i).startsWith("--change"))
			{
				if(Args.get(i).equals("--change"))
				{
					if(Args.get(i) != null)
					{
						i++;
					}
				}
			}
			else
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		for(int i = 0; i < tempArgs.size(); i++)
		{
			tempCommand += " " + tempArgs.get(i);
		}
		
		return tempCommand;
	}
	
	public static String getCommandWithoutLParam(String[] Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		String tempCommand = "";
		
		for(int i = 0; i < Args.length; i++)
		{
			if(Args[i].substring(0, 2).equals("-l"))
			{
				if(Args[i].equals("-l"))
				{
					if(Args[i + 1] != null)
					{
						i++;
					}
				}
			}
			else if(Args[i].startsWith("--limit"))
			{
				if(Args[i].equals("--limit"))
				{
					if(Args[i + 1] != null)
					{
						i++;
					}
				}
			}
			else
			{
				tempArgs.add(Args[i]);
			}
		}
		
		for(int i = 0; i < tempArgs.size(); i++)
		{
			tempCommand += " " + tempArgs.get(i);
		}
		
		return tempCommand;
	}
	
	public List<String> getArgsWithoutLParam(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).startsWith("-l") || Args.get(i).startsWith("--limit"))
			{
				if(Args.get(i).equals("-l") || Args.get(i).equals("--limit"))
				{
					if(Args.get(i + 1) != null)
					{
						i++;
					}
				}
			}
			else
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		return tempArgs;
	}
	
	public List<String> getArgsWithoutRParam(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).startsWith("-r") || Args.get(i).startsWith("--revision"))
			{
				if(Args.get(i).equals("-r") || Args.get(i).equals("--revision"))
				{
					if(Args.get(i + 1) != null)
					{
						i++;
					}
				}
			}
			else
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		return tempArgs;
	}
	
	public List<String> getArgsWithoutCParam(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		
		for(int i = 0; i < Args.size(); i++)
		{
			if(Args.get(i).startsWith("-c") || Args.get(i).startsWith("--change"))
			{
				if(Args.get(i).equals("-c") || Args.get(i).equals("--change"))
				{
					if(Args.get(i + 1) != null)
					{
						i++;
					}
				}
			}
			else
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		return tempArgs;
	}
	
	public List<String> getArgsWithoutPath(List<String> Args)
	{
		List<String> tempArgs = new ArrayList<String>();
		
		for(int i = 0; i < Args.size(); i++)
		{
			boolean argIsPath = false;
			if(!Args.get(i).startsWith("-"))
			{
				if(i == 2)
				{
					argIsPath = true;
				}
				else if(i > 2)
				{
					if(Args.get(i - 1).equals("-r") ||
						Args.get(i - 1).equals("--revision") ||
						Args.get(i - 1).equals("-c") ||
						Args.get(i - 1).equals("--change") ||
						Args.get(i - 1).equals("--targets") ||
						Args.get(i - 1).equals("-l") ||
						Args.get(i - 1).equals("--limit") ||
						Args.get(i - 1).equals("--with-revprop") ||
						Args.get(i - 1).equals("--depth") ||
						Args.get(i - 1).equals("--diff-cmd") ||
						Args.get(i - 1).equals("-x") ||
						Args.get(i - 1).equals("--extentions") ||
						Args.get(i - 1).equals("--username") ||
						Args.get(i - 1).equals("--password") ||
						Args.get(i - 1).equals("--config-dir") ||
						Args.get(i - 1).equals("--config-option"))
					{
						argIsPath = false;
					}
					else
					{
						argIsPath = true;
					}
				}
			}
			
			if(!argIsPath)
			{
				tempArgs.add(Args.get(i));
			}
		}
		
		return tempArgs;
	}
	
	
	public String setCommand(List<String> Args)
	{
		String tempCommand = "";
		
		for(int i = 0; i < Args.size(); i++)
		{
			tempCommand += " " + Args.get(i);
		}
		
		return tempCommand;
	}
}

