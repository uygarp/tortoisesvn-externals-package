import java.util.List;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.TableColumn;


public class TortoiseProcExtentionWindow
{

	protected Shell shell;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args)
	{
		try
		{
			TortoiseProcExtentionWindow window = new TortoiseProcExtentionWindow();
			window.open();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open()
	{
		Display display = Display.getDefault();
		createContents();
		fillTable();
		
		shell.open();
		shell.layout();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents()
	{
		shell = new Shell();
		shell.setSize(636, 556);
		shell.setText("SWT Application");
		shell.setLayout(new FormLayout());
		
		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		FormData fd_table = new FormData();
		fd_table.top = new FormAttachment(0, 27);
		fd_table.left = new FormAttachment(0, 24);
		table.setLayoutData(fd_table);
		formToolkit.adapt(table);
		formToolkit.paintBordersFor(table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnRevision = new TableColumn(table, SWT.NONE);
		tblclmnRevision.setWidth(100);
		tblclmnRevision.setText("Revision");
		
		TableColumn tblclmnAuthor = new TableColumn(table, SWT.NONE);
		tblclmnAuthor.setWidth(100);
		tblclmnAuthor.setText("Author");
		
		TableColumn tblclmnMessage = new TableColumn(table, SWT.NONE);
		tblclmnMessage.setWidth(304);
		tblclmnMessage.setText("Message");

	}

	private static List<SvnLog> svnLogList = null;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private Table table;

	public void setLogList(List<SvnLog> logList)
	{
		svnLogList = logList;
	}
	
	protected void fillTable()
	{
		Object[][] data =  {{"yeni","eski","yeni"}, {"yeni2","eski2","yeni2"}};
		table.setData(data);
	}
}
