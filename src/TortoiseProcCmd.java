
public class TortoiseProcCmd
{
	String svnCmd = "TortoiseProc ";
	
	private static TortoiseProcCmd instance = null;

	public static TortoiseProcCmd getInstance()
    {
       if(instance == null)
       {
          instance = new TortoiseProcCmd();
       }
       return instance;
    }

	public void setCmd(String value)
	{
		svnCmd = value + " ";
	}
	
	public String getCmd()
	{
		return svnCmd;
	}
}
