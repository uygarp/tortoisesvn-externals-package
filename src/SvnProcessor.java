import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.List;


public class SvnProcessor
{
	private static Process process;
		
	private static SvnProcessor instance = null;
	private static BufferedReader outputReader = null;
	private static BufferedReader errorReader = null;
	private static PrintStream printStream = null;
	private static SvnCmd svn = SvnCmd.getInstance();
    
	protected SvnProcessor()
    {
    }
    
    public static SvnProcessor getInstance()
    {
       if(instance == null)
       {
          instance = new SvnProcessor();
       }
       return instance;
    }
	
	
	void setCommand(String command) throws IOException
	{
		process = Runtime.getRuntime().exec(command);
		
		outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		//Output Encoding
		printStream = new PrintStream(System.out, true, "ibm857");
		//printStream = new PrintStream(System.out, true, "utf-8");
		//printStream = new PrintStream(System.out, true, "IBM1026");
		//printStream = new PrintStream(System.out, true, "windows-1254");
	    	
	}
	
	String readLine() throws IOException
	{
		String outputLine = outputReader.readLine();
		if(outputLine == null)
		{
			outputLine = errorReader.readLine();
			
			if(outputLine == null)
			{
				try
		        {
					process.waitFor();
		        }
		        catch (InterruptedException e)
		        {
		            e.printStackTrace();
		        }
			}
		}
			
		return outputLine;
	}
	
	public String runSvnCommand(String command) throws IOException
	{
		setCommand(command);
		String line = readLine();
		StringBuilder commandOut = new StringBuilder();
		
		while(line != null)
		{
			commandOut.append(line + "\n");
			line = readLine();
		}
		
		return commandOut.toString();
	}
	
	public int runSvnCommand(List<String> Args) throws IOException
	{
		String command = svn.getCmd();
		
		if(Args != null)
		{
			for(int i = 0; i < Args.size(); i++)
			{
				command += " " + Args.get(i);
			}
			
			return runCommand(command);
		}
		else
		{
			return GLOBAL.EXIT_FAILURE;
		}		
	}
	
	public int runCommand(String command) throws IOException
	{
		if(command == null || command == "")
		{
			return GLOBAL.EXIT_FAILURE;
		}
		
		setCommand(command);
		
		String line = readLine();		
		
		while(line != null)
		{
			printlnConsole(line);
			line = readLine();
		}
		
		return process.exitValue();
	}
	
	
	public boolean isLogCommandValid(String command) throws IOException
	{
		setCommand(command + " -l1");
		String line = readLine();		
		
		while(line != null)
		{
			if(errorCheck(line))
			{
				return false;
			}
			
			line = readLine();
		}
		
		return true;
	}
	
	public boolean errorCheck(String line)
	{
		if(line != null)
		{
			if(!line.startsWith("svn:"))
			{
				return false;
			}
		}
		
		return true;
	}
	
	private static void printlnConsole(String text)
	{
		printStream.println(text);
		//System.out.println(text);
	}
}
